<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Blog
 */

namespace Swissclinic\AmastyBlogExtension\Model;

use Amasty\Blog\Model\Repository\CategoriesRepository;
use Amasty\Blog\Model\Repository\CategoriesRepositoryFactory;

class Posts extends \Amasty\Blog\Model\Posts
{
    /**
     * @var CategoriesRepository
     */
    private $_categoriesRepo;

    public function __construct(
        CategoriesRepositoryFactory $catRepoFactory,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    )
    {
        $this->_categoriesRepo = $catRepoFactory->create();
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * @return string
     */
    public function getListThumbnailSrc()
    {
        $src = $this->getData('list_thumbnail') ? $this->getData('list_thumbnail') : $this->getData('post_thumbnail');
        return $this->getData('image_helper')->getImageUrl($src);
    }

    /**
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCategoriesName()
    {
        if (!$this->hasData('categories_name')) {
            $cats = $this->_categoriesRepo->getCategoriesByPost($this->getId());
            $catsArray = [];
            foreach ($cats as $cat) {
                $catsArray[] = $cat['name'];
            }
            $this->setData('categories_name', $catsArray);
        }

        return $this->_getData('categories_name');
    }
}
