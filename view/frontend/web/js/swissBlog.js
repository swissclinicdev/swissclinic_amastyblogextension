/**
 * Copyright © Swiss Clinic, Inc. All rights reserved.
 */
define(['jquery'], function($) {
    'use strict';

    $.widget('swissclinic.swissBlog', {
        /**
         * Initialize swissBlog
         * @private
         */
        _create: function() {
            const $window = $(window);
            const blogCategories = $('.amblog-categories');
            const blogCategoriesHeight = $('.amblog-categories').outerHeight();
            const headerHeight = $('.page-header').outerHeight();
            const magazineHeaderHeight = $('.magazine-header').outerHeight();

            // if ($window.width() < 768) {
            //     $window.scroll(function() {
            //         if ($(this).scrollTop() >= magazineHeaderHeight) {
            //             blogCategories.addClass('fixed');
            //             $('.magazine-header').css('margin-bottom', blogCategoriesHeight);
            //         } else {
            //             blogCategories.removeClass('fixed');
            //             $('.magazine-header').css('margin-bottom', '0');
            //         }
            //     });

            //     $('.widget-product-grid').scrollLeft(150);
            // }
        },
    });

    return $.swissclinic.swissBlog;
});
