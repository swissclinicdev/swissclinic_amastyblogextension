<?php

namespace Swissclinic\AmastyBlogExtension\Block\Sidebar;

use Amasty\Blog\Model\Posts;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

/**
 * Wrapper class for \Amasty\Blog\Block\Sidebar\Recentpost
 *
 * @package Swissclinic\AmastyBlogExtension\Block\Sidebar
 * @method string getBlockHeader()
 * @method string toHtml() @throws
 * @method bool showImages()
 * @method \Amasty\Blog\Model\ResourceModel\Posts\Collection getCollection()
 * @method bool needShowThesis()
 * @method bool needShowDate()
 * @method bool hasThumbnail(Posts $post)
 * @method \Magento\Framework\Registry getRegistry()
 * @method \Amasty\Blog\Api\CategoryRepositoryInterface getCategoryRepository
 * @method bool getDisplay()
 * @see \Amasty\Blog\Block\Sidebar\Recentpost
 */
class Recentpost extends Template implements BlockInterface
{
    /**
     * @var \Amasty\Blog\Block\Sidebar\Recentpost
     */
    private $_recentPost;

    /**
     * @var \Amasty\Blog\Helper\Image
     */
    private $_imageHelper;

    public function __construct(
        \Amasty\Blog\Block\Sidebar\Recentpost $recentPost,
        \Amasty\Blog\Helper\Image $imageHelper,
        Template\Context $context,
        array $data = []
    )
    {
        $this->_recentPost = $recentPost;
        $this->_imageHelper = $imageHelper;
        parent::__construct($context, $data);
    }

    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate("Swissclinic_AmastyBlogExtension::sidebar/recentpost.phtml");
    }

    /**
     * Altered function from \Amasty\Blog\Block\Sidebar\Recentpost
     * @param Posts $post
     * @return bool|string
     */
    public function getThumbnailSrc($post)
    {
        $src = $post->getListThumbnail() ? $post->getListThumbnail() : $post->getPostThumbnail();
        if ($src){
            try {
                $result = $this->_imageHelper->imageResize($src, 700, 350);
                return $result;
            } catch (NoSuchEntityException $e) {
                return false;
            }
        }

        return false;
    }

    /**
     * Wrapper function for \Amasty\Blog\Block\Sidebar\Recentpost methods
     * @param string $method
     * @param array $args
     * @return mixed
     */
    public function __call($method, $args)
    {
        return call_user_func_array(array($this->_recentPost, $method), $args);
    }
}

?>