<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Blog
 */

namespace Swissclinic\AmastyBlogExtension\Block;

class Layout extends \Amasty\Blog\Block\Layout
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate("Swissclinic_AmastyBlogExtension::layout.phtml");
    }
}