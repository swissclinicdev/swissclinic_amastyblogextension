<?php

namespace Swissclinic\AmastyBlogExtension\Block\Widgets\Blog;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use Amasty\Blog\Model\Posts as PostsModel;

class Posts extends Template implements BlockInterface
{
    /**
     * @var \Amasty\Blog\Model\ResourceModel\Posts\Collection
     */
    protected $_collection;

    /**
     * Posts constructor.
     * @param Template\Context $context
     * @param PostsModel $postsModel
     */
    public function __construct(Template\Context $context, PostsModel $postsModel)
    {
        $this->setTemplate('Swissclinic_AmastyBlogExtension::widgets/posts.phtml');
        $this->_collection = $postsModel->getCollection();
        parent::__construct($context);
    }

    /**
     * Set Categories filter on collection.
     */
    protected function _setCategoriesFilter()
    {
        $categories = $this->getData('categories');

        if ($categories) {
            $categories = explode(',', $categories);
            $blogCategoryTable = $this->_collection->getMainTable() . '_category';
            $this->_collection->getSelect()->join(
                ['categories' => $blogCategoryTable],
                "categories.post_id = main_table.post_id",
                []
            )->where("categories.category_id IN (?)", $categories);
        }
    }

    /**
     * Set Tags filter on collection.
     */
    protected function _setTagsFilter()
    {
        $tags = $this->getData('tags');

        if ($tags) {
            $tags = explode(',', $tags);
            $blogTagTable = $this->_collection->getMainTable();
            $this->_collection->getSelect()->join(
                ['tags' => $blogTagTable],
                "tags.post_id = main_table.post_id",
                []
            )->where("tags.tag_id IN (?)", $tags);
        }
    }

    /**
     * @return \Amasty\Blog\Model\ResourceModel\Posts\Collection|bool
     */
    public function getCollection()
    {
        if (!$this->_collection) {
            return false;
        }

        $this->_setCategoriesFilter();
        $this->_setTagsFilter();

        return $this->_collection;
    }
}