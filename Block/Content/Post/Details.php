<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Blog
 */

namespace Swissclinic\AmastyBlogExtension\Block\Content\Post;

class Details extends \Amasty\Blog\Block\Content\Post\Details
{
    public function getCategoriesHtml($isAmp = false)
    {
        $catDetails = $this->getLayout()->createBlock('Swissclinic\AmastyBlogExtension\Block\Content\Post\Details');
        if ($catDetails){
            $catDetails
                ->setPost($this->getPost())
                ->setTemplate('Swissclinic_AmastyBlogExtension::list/categories.phtml');
                ;
            return $catDetails->toHtml();
        }
        return false;
    }

}
